﻿capital = 622

oob = "PRC_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	mass_assault = 1
}
set_stability = 0.45
set_war_support = 0.45
add_timed_idea = { idea = Chinese_Red_Army days = 295 }
add_ideas = line_struggle1
country_event = { days = 295 id = RCM_comchina_west_expedition.1 }
drop_cosmetic_tag = yes
set_cosmetic_tag = PRC_CSR
save_global_event_target_as = WTT_communist_china
add_ideas = {
	prc_mao_zedong
	prc_bo_gu
	#prc_zhang_wentian
}
hidden_effect = {
	set_variable = { prc_mao_zedong_counter = 15 } #first increase after 15 days
	add_to_variable = { mao_total = 0.5 } #monthly for locs
}
hidden_effect = {
	set_variable = { prc_bo_gu_counter = 15 } #first increase after 15 days
	add_to_variable = { teb_total = 0.5 } #monthly for locs
}
1939.1.1 = {
	set_country_flag = j_focus_completed
	set_country_flag = nineteen_thirtynine_start
	set_cosmetic_tag = PRC_SGN
	add_political_power = 1198
	oob = "PRC_1939"
	remove_ideas = Chinese_Red_Army
	remove_ideas = PRC_Isolated_status
	remove_ideas = Long_March_1
	remove_ideas = line_struggle1
	#add_ideas = changjiang_bureau
	add_ideas = line_struggle3
	add_ideas = Long_March_3
	add_ideas = prc_ren_bishi
	add_ideas = PRC_kang_sheng
	# Chinese Popular Front effects
	add_to_war = {
		targeted_alliance = CHI
		enemy = JAP
	}
	set_global_flag = otto_braun_left_yanan
	set_global_flag = PRC_trotskyist_released
	set_global_flag = PRC_zhang_guotao_defect
	set_global_flag = zhou_enlai_in_moscow
	set_global_flag = ren_bishi_in_moscow
	set_global_flag = wang_jiaxiang_return_from_soviet
	set_global_flag = PRC_wang_ming_returned_flag
	set_global_flag = PRC_chen_yun_returned_flag
	set_global_flag = PRC_kang_sheng_returned_flag
	clr_global_flag = prc_wang_jiaxiang_go_to_soviet_union
	set_global_flag = prc_chen_changhao_go_to_soviet_union
	remove_opinion_modifier = { target = CHI modifier = hostile_status }
	unlock_national_focus = PRC_East_conquest
	unlock_national_focus = PRC_Western_expedition
	unlock_national_focus = PRC_End_of_Long_March
	unlock_national_focus = PRC_6th_plenary_session_of_the_6th_CPC_central_committee_focus
	unlock_national_focus = PRC_demand_another_politburo_meeting_focus
	unlock_national_focus = PRC_send_trotskyists_to_sinkiang_focus
	unlock_national_focus = PRC_question_of_chen_duxiu_focus
	unlock_national_focus = PRC_dispatch_ren_bishi_to_moscow_focus
	complete_national_focus = PRC_Red_Army_Recovery
	complete_national_focus = PRC_Peasant_class
	complete_national_focus = PRC_Land_reform
	unlock_national_focus = PRC_contact_Zhang_Xueliang_focus
	complete_national_focus = PRC_kangda_focus
	complete_national_focus = PRC_Join_the_war
	complete_national_focus = PRC_found_the_military_commission_general_political_department_focus
	complete_national_focus = PRC_Type_81_ma
	complete_national_focus = PRC_makeshift_research_facility
	
	#complete_national_focus =
	#complete_national_focus =
	#complete_national_focus =
	#complete_national_focus =
	#complete_national_focus =
	#complete_national_focus = PRC_End_of_Long_March
	#complete_national_focus = PRC_Farmer_political_course
	complete_national_focus = PRC_Red_Army


			#generic focuses

	# Chinese Popular Front effects

	remove_ideas = Long_March_2
	add_ideas = {
		#laws
		civilian_economy
		extensive_conscription

		
	}



	set_technology = {
		tech_mountaineers = 1
		#doctrines
		air_superiority = 1
		pocket_defence = 1
		defence_in_depth = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1
		computing_machine = 1
		basic_encryption = 1
		basic_decryption = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		fuel_silos = 1
		fuel_refining = 1
		construction1 = 1
		construction2 = 1
		construction3 = 1
		concentrated_industry = 1
		concentrated_industry2 = 1
		concentrated_industry3 = 1
	}
}

set_popularities = {
	democratic = 0
	n_socialism = 0
	fascism = 0
	communism = 100
	neutrality = 0
}
set_politics = {

	ruling_party = communism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

1939.1.1 = {
	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 100
	}
	set_politics = {

		ruling_party = communism
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
}

add_ideas = {
	export_focus
	limited_conscription
	#partial_economic_mobilisation
	civilian_economy
	PRC_communal_economy
	#PRC_Mobile_Warfare
	PRC_Isolated_status
	Long_March_1
	PRC_no_skilled_personnel_1
	no_proper_equipment_design
	#incompetent_heavy_industry
	unbalanced_industry
	iliterate_population
	PRC_Poor_Air
	PRC_Poor_navy
}

add_opinion_modifier = { target = CHI modifier = hostile_status }

create_country_leader = {
	name = "Mao Zedong"
	desc = "POLITICS_MAO_ZEDONG_DESC"
	picture = "Portrait_PRC_Mao_Zedong.dds"
	expire = "1965.1.1"
	ideology = maoism
	traits = {
		cornered_fox
	}
}
create_country_leader = {
	name = "Zhang Wentian"
	desc = "POLITICS_MAO_ZEDONG_DESC"
	picture = "Zhang_Wentian.dds"
	expire = "1965.1.1"
	ideology = marxism
	traits = {
		cornered_fox
	}
}

create_country_leader = {
	name = "None"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
	}
}
create_country_leader = {
	name = "None"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = conservatism
	traits = {
	}
}
create_country_leader = {
	name = "Shen Junru"
	desc = ""
	picture = "Portrait_China_Shen_Junru.dds"
	expire = "1965.1.1"
	ideology = n_socialism_ideology
	traits = {
		#
	}
}
create_country_leader = {
	name = "None"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = despotism
	traits = {
	}
}


create_field_marshal = {
	name = "Zhu De"
	picture = "Portrait_PRC_Zhu_De.dds"
	traits = { old_guard organisational_leader inspirational_leader expert_delegator }
	id = 9003
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Peng Dehuai"
	picture = "Portrait_PRC_Peng_Dehuai.dds"
	traits = { war_hero infantry_officer }
	id = 9004
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Lin Biao"
	picture = "Portrait_PRC_Lin_Biao.dds"
	traits = { brilliant_strategist career_officer infantry_officer }
	id = 9000
	skill = 4
	attack_skill = 3 #4
	defense_skill = 4
	planning_skill = 3 #4
	logistics_skill = 3
}
create_corps_commander = {
	name = "Liu Bocheng"
	picture = "Portrait_PRC_Liu_Bocheng.dds"
	traits = { brilliant_strategist war_hero infantry_officer }
	id = 9001
	skill = 4
	attack_skill = 3 #4
	defense_skill = 3
	planning_skill = 3 #4
	logistics_skill = 3
}
create_corps_commander = {
	name = "He Long"
	picture = "Portrait_PRC_He_Long.dds"
	traits = { war_hero infantry_officer }
	id = 9002
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 4
}
create_corps_commander = {
	name = "Luo Ronghuan"
	picture = "Portrait_PRC_Luo_Ronghuan.dds"
	traits = { politically_connected }
	id = 9007
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 4
}
create_corps_commander = {
	name = "Xu Xiangqian"
	picture = "Portrait_PRC_Xu_Xiangqian.dds"
	traits = { infantry_officer }
	id = 9005
	skill = 3
	attack_skill = 4
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 2
}
create_corps_commander = {
	name = "Nie Rongzhen"
	picture = "Portrait_PRC_Nie_Rongzhen.dds"
	traits = { career_officer  }
	id = 9008
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Ye Jianying"
	picture = "Portrait_PRC_Ye_Jianying.dds"
	traits = { career_officer }
	id = 9006
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 3
}
create_corps_commander = {
	name = "Chen Geng"
	picture = "Portrait_PRC_Chen_Geng.dds"
	traits = {}
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Xiao Jinguang"
	picture = "Portrait_PRC_Xiao_Jinguang.dds"
	traits = {}
	skill = 2
	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 2
}
create_corps_commander = {
	name = "Xiao Ke"
	picture = "Portrait_PRC_Xiao_Ke.dds"
	traits = {}
	skill = 2
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}
