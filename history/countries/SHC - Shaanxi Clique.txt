﻿capital = 861

oob = "SHC_1936"


# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	gwtank = 1
	early_fighter = 1
	early_bomber = 1
	mass_assault = 1
	#fleet_in_being = 1
	#early_destroyer = 1

}

add_war_support = 0.15
add_stability = 0.25
set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	democratic = 0
	fascism = 0
	communism = 0
	neutrality = 100

}
set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Jing Yuexiu"
	desc = "POLITICS_JING_YUEXIU_DESC"
	picture = "Portrait_Shaanxi_Jing_Yuexiu.dds"
	expire = "1960.7.22"
	ideology = despotism
	traits = {
		#xishan_doctrine
	}
}

create_country_leader = {
  name = "Jing Yuexiu"
	desc = "POLITICS_JING_YUEXIU_DESC"
	picture = "Portrait_Shaanxi_Jing_Yuexiu.dds"
	expire = "1970.11.3"
	ideology = fascism_ideology
	traits = {
	}
}

create_country_leader = {
   name = "Jing Yuexiu"
	desc = "POLITICS_JING_YUEXIU_DESC"
	picture = "Portrait_Shaanxi_Jing_Yuexiu.dds"
	expire = "1970.11.3"
	ideology = conservatism
	traits = {
	}
}

create_country_leader = {
    name = "Jing Yuexiu"
	desc = "POLITICS_JING_YUEXIU_DESC"
	picture = "Portrait_Shaanxi_Jing_Yuexiu.dds"
	expire = "1970.11.3"
	ideology = n_socialism_ideology
	traits = {
	}
}

create_country_leader = {
 	name = "Jing Yuexiu"
	desc = "POLITICS_JING_YUEXIU_DESC"
	picture = "Portrait_Shaanxi_Jing_Yuexiu.dds"
	expire = "1970.11.3"
	ideology = marxism
	traits = {
	}
}

create_field_marshal = {
	name = "Gao Shuangcheng"
	portrait_path = "gfx/leaders/SHC/Portrait_Shaanxi_Gao_Shuangcheng.dds"
	traits = { defensive_doctrine }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}
create_corps_commander = {
	name = "Gao Guizhi"
	portrait_path = "gfx/leaders/SHC/Portrait_Shaanxi_Gao_Guizhi.dds"
	traits = {  }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}
create_corps_commander = {
	name = "Sun Weiru"
	portrait_path = "gfx/leaders/NEA/China_Shaanxi_Sun_Weiru.dds"
	traits = {  }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

