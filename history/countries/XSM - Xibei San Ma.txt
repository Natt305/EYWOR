﻿capital = 863 #MODDED

oob = "XSM_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	gw_artillery = 1
	mass_assault = 1
}
add_ideas = MON_nomadic #MODDED
1939.1.1 = {
	
	create_country_leader = { #MODDED
	name = "Ma Bufang"
		desc = "POLITICS_MA_BUFANG_DESC"
		picture = "Portrait_Xibei_San_Ma_Ma_Bufang.dds"
		expire = "1970.11.3"
		ideology = despotism
	}
	add_to_war = { #MODDED
		targeted_alliance = CHI
		enemy = JAP
	}
 #MODDED
	add_political_power = 1198

			#generic focuses
	if = {
		limit = {
			NOT = { #MODDED
				has_dlc = "Waking the Tiger"
			} #MODDED
		} #MODDED
		complete_national_focus = army_effort
		complete_national_focus = equipment_effort
		complete_national_focus = motorization_effort
		complete_national_focus = aviation_effort
		complete_national_focus = construction_effort_2
		complete_national_focus = production_effort_2
		complete_national_focus = infrastructure_effort
		complete_national_focus = industrial_effort
		complete_national_focus = construction_effort
		complete_national_focus = production_effort
	}

	add_ideas = {
		#laws
		tot_economic_mobilisation
		service_by_requirement
		closed_economy
	}

	oob = "XSM_1939"

	set_technology = {
		#doctrines
		air_superiority = 1
		pocket_defence = 1
		defence_in_depth = 1
		fleet_in_being = 1
		battlefleet_concentration = 1
		convoy_sailing = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1
		computing_machine = 1
		basic_encryption = 1
		basic_decryption = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		fuel_silos = 1
		construction1 = 1
		construction2 = 1
		construction3 = 1
		concentrated_industry = 1
		concentrated_industry2 = 1
		concentrated_industry3 = 1
	}
}

set_popularities = { #MODDED
	democratic = 0
	fascism = 0
	communism = 0
	neutrality = 100

}
set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
create_country_leader = { #MODDED
	name = "Ma Lin"
	desc = "POLITICS_MA_LIN_DESC"
	picture = "Portrait_Xibei_San_Ma_Ma_Lin.dds"
	expire = "1975.7.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = { #MODDED
	name = "Ma Buqing"
	desc = "POLITICS_MA_BUQING_DESC"
	picture = "Portrait_Xibei_San_Ma_Ma_Buqing.dds"
	expire = "1975.7.1"
	ideology = fascism_ideology
	traits = {
		#
	}
}
create_country_leader = {
	name = "Gao Shuxun"
	desc = "POLITICS_GAO_SHUXUN_DESC"
	picture = "Portrait_Xibei_San_Ma_Gao_Shuxun.dds"
	expire = "1975.7.1"
	ideology = marxism
	traits = {
		#
	}
}

create_country_leader = { #MODDED
  name = "Ma Bufang"
	desc = "POLITICS_MA_BUFANG_DESC"
	picture = "Portrait_Xibei_San_Ma_Ma_Bufang.dds"
	expire = "1970.11.3"
	ideology = fascism_ideology
}

create_country_leader = { #MODDED
  name = "Ma Bufang"
	desc = "POLITICS_MA_BUFANG_DESC"
	picture = "Portrait_Xibei_San_Ma_Ma_Bufang.dds"
	expire = "1970.11.3"
	ideology = conservatism
}

create_country_leader = { #MODDED
  name = "Ma Bufang"
	desc = "POLITICS_MA_BUFANG_DESC"
	picture = "Portrait_Xibei_San_Ma_Ma_Bufang.dds"
	expire = "1970.11.3"
	ideology = n_socialism_ideology
}

create_country_leader = { #MODDED
 	name = "Ma Bufang"
	desc = "POLITICS_MA_BUFANG_DESC"
	picture = "Portrait_Xibei_San_Ma_Ma_Bufang.dds"
	expire = "1970.11.3"
	ideology = marxism
}


create_field_marshal = { #MODDED
	name = "Ma Bufang"
	portrait_path = "gfx/leaders/XSM/Portrait_Xibei_San_Ma_Ma_Bufang.dds"
	traits = { offensive_doctrine trait_reckless }
	id = 3003
	skill = 4
	attack_skill = 3
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 3
}
create_field_marshal = { #MODDED
	name = "Ma Buqing"
	id = 3004
	portrait_path = "gfx/leaders/XSM/Portrait_Xibei_San_Ma_Ma_Buquing.dds"
	traits = { cavalry_officer politically_connected }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}
