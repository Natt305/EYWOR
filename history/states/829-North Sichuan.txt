
state={
	id=829
	name="STATE_829"

	history={
		owner = SIC
		add_core_of = SIC
		add_core_of = CHI
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			arms_factory = 1

		}
		victory_points = {
			4925 20 
		}
		1939.1.1 = {
			remove_core_of = SIC
			owner = CHI
			buildings = {
				industrial_complex = 2
				infrastructure = 6
				arms_factory = 1

			}

		}

	}

	provinces={
		1257 1367 2030 2045 2091 4375 4403 4925 5193 7256 8026 9966 10132 10787 10903 12274 12767 
	}
	manpower=13297000
	buildings_max_level_factor=1.000
	state_category=large_city
}
