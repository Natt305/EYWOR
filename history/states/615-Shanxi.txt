
state={
	id = 615
	name="STATE_615"
	resources={
		steel=5.000
	}

	history={
		owner = SHX
		add_core_of = SHX
		add_core_of = CHI
		
		victory_points = {
			1519 3 
		}
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 1

		}
		1939.1.1 = {
			
			add_core_of = EHB
			owner = SHX
			controller = EHB
		}

	}

	provinces={
		1027 1203 1438 1519 4174 4469 4538 12078 12344 
	}
	manpower=7201000
	buildings_max_level_factor=1.000
	state_category=city
}
