﻿division_template = {
	name = "Luchün Shih"				# Represents a corps of 3 NRA divisions (2 brigades of 2 regiments each)
	division_names_group = CHI_INF_01								# Militia-level training and equipment
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Ch'iping Shih"			# Cavalry Division
	division_names_group = CHI_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
	}
}
units = {
	#1st District Army "Fengtien"
	division = {
		name = "Luchün 1. Shih"
		location = 11752
		division_template = "Luchün Shih"
		start_experience_factor = 0.2
		start_equipment_factor = 0.6
	}
	division = {
		name = "Luchün 2. Shih"
		location = 11752
		division_template = "Luchün Shih"
		start_experience_factor = 0.2
		start_equipment_factor = 0.6
	}
	division = {
		name = "Luchün 3. Shih"
		location = 11752
		division_template = "Luchün Shih"
		start_experience_factor = 0.2
		start_equipment_factor = 0.4
	}
	division = {
		name = "Ch'iping 1. Shih"
		location = 4525
		division_template = "Ch'iping Shih"
		start_equipment_factor = 0.4
		start_experience_factor = 0.3
	}
	#division = {
	#	name = "Ch'iping 2. Shih"
	#	location = 10424
	#	division_template = "Ch'iping Shih"
	#	start_equipment_factor = 0.5
	#	start_experience_factor = 0.2
	#}

}

##### STARTING PRODUCTION #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "PLM"
		}
		requested_factories = 1
		progress = 0.82
		efficiency = 100
	}
}
