﻿division_template = {
	name = "Luchün Shih"			
	division_names_group = XSM_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
	}
	support = {
	}
}

division_template = {
	name = "T'iaocheng Shih"			
	division_names_group = XSM_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
	}
	support = {
		artillery = { x = 0 y = 0 } 
	}
}

division_template = {
	name = "Chengli Shih"			
	division_names_group = XSM_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		artillery_brigade = { x = 4 y = 0 }
	}
	support = {
		engineer = { x = 0 y = 0 } 
		artillery = { x = 0 y = 1 } 
	}
}

division_template = {
	name = "Ch'iping Shih"	
	division_names_group = XSM_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 3 y = 0 }
		cavalry = { x = 3 y = 1 }
	}
	support = {
	}
}

division_template = {
	name = "Tuli Lü"	
	division_names_group = CHI_INF_02

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
	support = {
	}
}

division_template = {
	name = "Tuli Ch'iping Lü"	
	division_names_group = CHI_CAV_02

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
	}
}

units = {
	division = {
		name = "100. Shih Ti-i Lü"
		location = 12732
		division_template = "Tuli Ch'iping Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.2
	}
	
	division = {
		name = "100. Shih Ti-erh Lü"
		location = 12732
		division_template = "Tuli Ch'iping Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.2
	}
	
	division = {
		name = "100. Shih Ti-san Lü"
		location = 12732
		division_template = "Tuli Ch'iping Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.2
	}
	
	division = {
		name = "Luchün Ti 5 Ch'iping Shih"
		location = 12732
		division_template = "Ch'iping Shih"
		start_equipment_factor = 0.2
		start_experience_factor = 0.2
	}
	
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 4964
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12732
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12899
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 7727
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 1778
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12596
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 8102
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 5057
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 5100
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 7971
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12890
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 8037
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12846
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 10490
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12327
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 7940
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 2053
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 4914
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 10865
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 2099
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12890
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 1882
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 2028
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12820
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 5076
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12820
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 2028
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 7997
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 12815
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 10834
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 8018
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 7270
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 11448
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 4536
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 10750
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 4888
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
	division = {
		name = "Tsinghai Pao-an Tui"
		location = 10817
		division_template = "Tuli Lü"
		start_equipment_factor = 0.45
		start_experience_factor = 0.1
	}
}

### STARTING PRODUCTION ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "XSM"
		}
		requested_factories = 1
		progress = 0.74
		efficiency = 100
	}
}