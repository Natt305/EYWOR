﻿division_template = {
	name = "Zhidui"
	#division_names_group = PRC_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 3 y = 0 }
	}
	#priority = 1
}
### OOB ###
units = {
	# N4A #
	division = { # "N4A1D"
		name = "Xinsijun Di 1 Zhidui"
		#location = 11988 #江西-湘鄂赣边
		location = 7208
		division_template = "Zhidui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.1
		start_manpower_factor = 0.01
	}
	division = { # "N4A2D"
		name = "Xinsijun Di 2 Zhidui"
		#location = 9972 #浙南-闽浙边
		location = 7208
		division_template = "Zhidui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
		start_manpower_factor = 0.01
	}
	division = { # "N4A3D"
		name = "Xinsijun Di 3 Zhidui"
		#location = 10012 #闽北
		location = 8049
		division_template = "Zhidui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
		start_manpower_factor = 0.01
	}
	division = { # "N4A4D"
		name = "Xinsijun Di 4 Zhidui"
		#location = 4158 #皖西-鄂豫皖边
		location = 8049
		division_template = "Zhidui"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
		start_manpower_factor = 0.01
	}
}