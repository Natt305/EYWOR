﻿division_template = {
	name = "Bubing Lü"				# Represents: German-trained divisions (better equipment than rest),
									# 	as well as two-division infantry corps (generally poorly-equipped militias),
	regiments = {					#	and local militia groups.
		infantry = { x = 0 y = 0 }	# Note: Chinese divisions were brigade-sized compared other nations' armies
		infantry = { x = 0 y = 1 }
	}
}

units = {
division = {
		name = "1st bubing Lü"
		location = 11913
		division_template = "Bubing Lü"
}
division = {
		name = "2nd bubing Lü"
		location = 11913
		division_template = "Bubing Lü"
}
division = {
		name = "3rd bubing Lü"
		location = 11913
		division_template = "Bubing Lü"
}
division = {
		name = "4th bubing Lü"
		location = 11913
		division_template = "Bubing Lü"
}
division = {
		name = "5th bubing Lü"
		location = 7014
		division_template = "Bubing Lü"
}
division = {
		name = "6th bubing Lü"
		location = 7014
		division_template = "Bubing Lü"
}
}

##### STARTING PRODUCTION #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_0
			creator = "RFM"
		}
		requested_factories = 1
		progress = 0.82
		efficiency = 100
	}
}
