﻿# Division template historical names system. Is a new method of naming the divisions based on the names-group assigned to it's template.
# If we run out of all historical names, the names will be assigned in the old way.
#
# Each group has a made up tag. Use it if you want to append more, or replace existing names with the extra txt files (modding).
#
# for_countries - lists all countries that can use it. If empty, or entire tag is missing, all countries in the world can use it.
#
# can_use - is a trigger that locks/unlocks the group under specific circumstances. The trigger is in a country scope.
#
# division_types - is a list of tokens to corresponding unit types. A player can in fact use any group of names for a div.template
#                  however this tag is a helper for an automated choice (for AI, or if the group must switch on it's own, because
#                  for example the current one is no longer available due to the can_use trigger saying so).
#                  In automated choice, the division template must have at least 1 of the following types for it to be chosen.
#
# fallback_name - Is going to be used if we run out of the scripted historical names. If you want to use the old division naming
#                 mechanics to be used for fallbacks, then just skip this option.
#
# unordered - It's a list of historical division names that did not have a number. Regardless if such names happened in history or not
#             this option is available here mainly for a mods.
#
# ordered - Is a list of all historical names. 
#           Numbers must start from 1 and up. 
#           Numbers can't repeat in one scope.
#           If you script the name for this group, at the same number (for example in a mod in another file), the name will be override.
#           All arguments between the brackets must be separated by spaces. Each argument is wrapped in "quotas".
#           1st argument = The name. It must contain either: 
#                          %d (for decimal number placement)
#                          %s (for string number placement - ROMAN numbers like XIV).
#           2nd argument is optional = A localizable text describing this historical division. The text will be visible in the tooltip
#                                      where you choose the historical division name.
#           3rd argument is optional = An URL preferably pointing to the WIKI. It's a future feature that is not currently working in
#                                      the current game version.
PRC_INF_01 = 
{
	name = "Infantry Divisions"

	for_countries = { PRC }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { PRC_INF_01 }

	fallback_name = "Bālù-Jūn Xinbian Di %d Lü"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		343 = { "Bālù-Jūn 115 Shi Di 343 Lü" }
		344 = { "Bālù-Jūn 115 Shi Di 344 Lü" }
		358 = { "Bālù-Jūn 120 Shi Di 358 Lü" }
		359 = { "Bālù-Jūn 120 Shi Di 359 Lü" }
		385 = { "Bālù-Jūn 129 Shi Di 385 Lü" }
		386 = { "Bālù-Jūn 129 Shi Di 386 Lü" }
		  1 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  2 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  3 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  4 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  5 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  6 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  7 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  8 = { "Bālù-Jūn Xinbian Di %d Lü" }
		  9 = { "Bālù-Jūn Xinbian Di %d Lü" }
		 10 = { "Bālù-Jūn Xinbian Di %d Lü" }
		 11 = { "Bālù-Jūn Di-yi Zongdui Di 1 Lü" }
		 12 = { "Bālù-Jūn Di-yi Zongdui Di 2 Lü" }
		 13 = { "Bālù-Jūn Di-yi Zongdui Di 3 Lü" }
		 14 = { "Bālù-Jūn Di-yi Zongdui Di 4 Lü" }
		 15 = { "Bālù-Jūn Di-yi Zongdui Di 5 Lü" }
		 16 = { "Bālù-Jūn Di-yi Zongdui Di 6 Lü" }
		 17 = { "Bālù-Jūn Di-yi Zongdui Di 7 Lü" }
		 18 = { "Bālù-Jūn Di-yi Zongdui Di 8 Lü" }
		 19 = { "Bālù-Jūn Di-yi Zongdui Di 9 Lü" }
		 20 = { "Bālù-Jūn Di-yi Zongdui Di 10 Lü" }
		 21 = { "Bālù-Jūn Di-er Zongdui Di 1 Lü" }
		 22 = { "Bālù-Jūn Di-er Zongdui Di 2 Lü" }
		 23 = { "Bālù-Jūn Di-er Zongdui Di 3 Lü" }
		 24 = { "Bālù-Jūn Di-er Zongdui Di 4 Lü" }
		 25 = { "Bālù-Jūn Di-er Zongdui Di 5 Lü" }
		 26 = { "Bālù-Jūn Di-er Zongdui Di 6 Lü" }
		 27 = { "Bālù-Jūn Di-er Zongdui Di 7 Lü" }
		 28 = { "Bālù-Jūn Di-er Zongdui Di 8 Lü" }
		 29 = { "Bālù-Jūn Di-er Zongdui Di 9 Lü" }
		 30 = { "Bālù-Jūn Di-er Zongdui Di 10 Lü" }
		 31 = { "Bālù-Jūn Di-san Zongdui Di 1 Lü" }
		 32 = { "Bālù-Jūn Di-san Zongdui Di 2 Lü" }
		 33 = { "Bālù-Jūn Di-san Zongdui Di 3 Lü" }
		 34 = { "Bālù-Jūn Di-san Zongdui Di 4 Lü" }
		 35 = { "Bālù-Jūn Di-san Zongdui Di 5 Lü" }
		 36 = { "Bālù-Jūn Di-san Zongdui Di 6 Lü" }
		 37 = { "Bālù-Jūn Di-san Zongdui Di 7 Lü" }
		 38 = { "Bālù-Jūn Di-san Zongdui Di 8 Lü" }
		 39 = { "Bālù-Jūn Di-san Zongdui Di 9 Lü" }
		 40 = { "Bālù-Jūn Di-san Zongdui Di 10 Lü" }
		 41 = { "Bālù-Jūn Di-si Zongdui Di 1 Lü" }
		 42 = { "Bālù-Jūn Di-si Zongdui Di 2 Lü" }
		 43 = { "Bālù-Jūn Di-si Zongdui Di 3 Lü" }
		 44 = { "Bālù-Jūn Di-si Zongdui Di 4 Lü" }
		 45 = { "Bālù-Jūn Di-si Zongdui Di 5 Lü" }
		 46 = { "Bālù-Jūn Di-si Zongdui Di 6 Lü" }
		 47 = { "Bālù-Jūn Di-si Zongdui Di 7 Lü" }
		 48 = { "Bālù-Jūn Di-si Zongdui Di 8 Lü" }
		 49 = { "Bālù-Jūn Di-si Zongdui Di 9 Lü" }
		 50 = { "Bālù-Jūn Di-si Zongdui Di 10 Lü" }
		 51 = { "Bālù-Jūn Di-wu Zongdui Di 1 Lü" }
		 52 = { "Bālù-Jūn Di-wu Zongdui Di 2 Lü" }
		 53 = { "Bālù-Jūn Di-wu Zongdui Di 3 Lü" }
		 54 = { "Bālù-Jūn Di-wu Zongdui Di 4 Lü" }
		 55 = { "Bālù-Jūn Di-wu Zongdui Di 5 Lü" }
		 56 = { "Bālù-Jūn Di-wu Zongdui Di 6 Lü" }
		 57 = { "Bālù-Jūn Di-wu Zongdui Di 7 Lü" }
		 58 = { "Bālù-Jūn Di-wu Zongdui Di 8 Lü" }
		 59 = { "Bālù-Jūn Di-wu Zongdui Di 9 Lü" }
		 60 = { "Bālù-Jūn Di-wu Zongdui Di 10 Lü" }
	}
}

PRC_INF_02 = 
{
	name = "Garrison Divisions"

	for_countries = { PRC }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { PRC_INF_01 }

	fallback_name = "Xīn Sì Jūn Di %d Zhidui"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		  1 = { "Xīn Sì Jūn Di 1 Zhidui" }
		  2 = { "Xīn Sì Jūn Di 2 Zhidui" }
		  3 = { "Xīn Sì Jūn Di 3 Zhidui" }
		  4 = { "Xīn Sì Jūn Di 4 Zhidui" }

	}
}

