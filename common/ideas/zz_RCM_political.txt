ideas = { #copy to zzz_generic when updating
	political_advisor = {
		# POLITICAL
		generic_communist_revolutionary = {
			
			allowed = {	#MODDEDvv, but needs edit when update
				NOT = { OR = { tag = PRC tag = GER tag = ENG tag = SOV original_tag = FRA tag = ITA tag = JAP tag = USA tag = AST tag = RAJ tag = CAN tag = SAF tag = CZE tag = ROM tag = CHI tag = MAN tag = MEX tag = HOL original_tag = SPR tag = ECA tag = EHB tag = RFM tag = SCG tag = CCP tag = NSG tag = FIC } }
				NOT = {
					has_available_idea_with_traits = { idea = communist_revolutionary limit = 1 }
				}
				NOT = {
					tag = PLM #MODDED
				}
				NOT = { is_originally_warlord = yes } #MODDED
				if = {
					limit = { has_dlc = "Battle for the Bosporus" }
					NOT = { tag = TUR tag = GRE }
				}
			}
			available = {
				if = {
					limit = {
						original_tag = NZL
						has_dlc = "Together for Victory"
					}
					has_completed_focus = NZL_the_lee_affair
				}
				if = {
					limit = { has_dlc = "Man the Guns" }
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
			}
			
			traits = { communist_revolutionary }
	
			on_add = {
				#country_event = political.1
			}
	
			do_effect = {
				NOT = {
					has_government = communism
				}
			}
	
			ai_will_do = {
				factor = 0
			}
		}
			
		generic_democratic_reformer = {
			allowed = { #MODDEDvv, but needs edit when update
				NOT = { OR = { tag = GER tag = ENG tag = SOV original_tag = FRA tag = ITA tag = JAP tag = USA tag = AST tag = RAJ tag = CAN tag = SAF tag = CZE tag = PRC tag = CHI tag = MAN tag = MEX tag = HOL original_tag = SPR tag = FIC } }
				NOT = {
					has_available_idea_with_traits = { idea = democratic_reformer limit = 1 }
				}
				NOT = {
					tag = PLM #MODDED
				}
				NOT = { is_originally_warlord = yes } #MODDED
				if = {
					limit = { has_dlc = "Battle for the Bosporus" }
					NOT = { tag = TUR tag = GRE }
				}
			}
			
			available = {
				if = {
					limit = {
						original_tag = NZL
						has_dlc = "Together for Victory"
					}
					OR = {
						has_completed_focus = NZL_strengthen_the_commonwealth
						has_completed_focus = NZL_constitution_amendment_act
					}
				}
				if = {
					limit = { has_dlc = "Man the Guns" }
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
				if = {
					limit = { has_dlc = "La Resistance" }	
					NOT = { original_tag = POR }
				}
				if = {
					limit = {	#MODDEDv
						original_tag = SIK
					}
					NOT = {
						has_country_leader = {
							name = "Sheng Shicai" ruling_only = yes
						}
					}
				}
				if = {
					limit = {
						OR = {
							original_tag = ECA
							original_tag = EHB
							original_tag = RFM
							original_tag = CCP
							original_tag = NSG
							original_tag = SCG
						}
					}
					is_puppet = no
				}	#MODDED^
			}

			
			traits = { democratic_reformer }
	
			on_add = {
				#country_event = political.13
			}
	
			do_effect = {
				NOT = {
					has_government = democratic
				}
			}
	
			ai_will_do = {
				factor = 0
			}
		}
		
		generic_fascist_demagogue = {
			
			allowed = {	#MODDEDvv, but needs edit when update
				NOT = { OR = { tag = GER tag = ENG tag = SOV original_tag = FRA tag = ITA tag = JAP tag = USA tag = AST tag = RAJ tag = CAN tag = SAF tag = ROM tag = CZE tag = PRC tag = CHI tag = MAN tag = MEX tag = HOL original_tag = SPR original_tag = POR tag = FIC } }
				NOT = {	#MODDED^^
					has_available_idea_with_traits = { idea = fascist_demagogue limit = 1 }
				}
				NOT = {
					tag = PLM #MODDED
				}
				NOT = { is_originally_warlord = yes } #MODDED
				if = {
					limit = { has_dlc = "Battle for the Bosporus" }
					NOT = { tag = TUR tag = GRE }
				}
			}
			
			available = {
				if = {
					limit = {
						original_tag = NZL
						has_dlc = "Together for Victory"
					}
					has_completed_focus = NZL_in_the_darkness
				}
				if = {
					limit = { has_dlc = "Man the Guns" }
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
				if = { #MODDED
					limit = {
						original_tag = SIK
					}
					NOT = {
						has_country_leader = {
							name = "Sheng Shicai" ruling_only = yes
						}
					}
				} #MODDED
			}
			
			traits = { fascist_demagogue }
	
			on_add = {
				#country_event = political.7
			}
	
			do_effect = {
				NOT = {
					has_government = fascism
				}
			}
			ai_will_do = { #MODDED
				factor = 0
			}
		}
		generic_socialist_idealist = {
			allowed = { #MODDED
				#MODDEDvvvvv
				#NOT = { is_originally_warlord = yes } #MODDED
				#NOT = { OR = { tag = GER tag = ENG tag = SOV tag = CHI tag = FRA tag = ITA tag = JAP tag = USA } } #MODDED
				NOT = {
					is_originally_warlord = yes #MODDED
					tag = PLM #MODDED
					tag = CHI #MODDED
					tag = USA #MODDED
					tag = MAN #MODDED
					tag = JAP #MODDED
					tag = PRC #MODDED
					tag = ECA #MODDED
					tag = EHB #MODDED
					tag = RFM #MODDED
					tag = SCG #MODDED
					tag = CCP #MODDED
					tag = NSG #MODDED
					tag = FIC #MODDED
				} #MODDED
			} #MODDED
			available = {
				if = {
					limit = { has_dlc = "Man the Guns" }
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
				if = {
					limit = {
						original_tag = SIK
					}
					NOT = {
						has_country_leader = {
							name = "Sheng Shicai" ruling_only = yes
						}
					}
				}
			}
			visible = {
				NOT = {
					has_available_idea_with_traits = { idea = socialist_idealist limit = 1 }
				}
			}
			traits = { socialist_idealist }

			ai_will_do = {
				factor = 0
			}
		}
	}
}
