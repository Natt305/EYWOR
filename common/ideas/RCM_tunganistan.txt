ideas = {
	political_advisor = {
		
		TNG_pierre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { backroom_backstabber }
		}

		TNG_psre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { compassionate_gentleman }
		}

		TNG_pierre_krier = {

			picture = generic_political_advisor_asia_3
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { captain_of_industry }
		}
	}
	# MITNGARY
	army_chief = {
		
		TNG_emile_speller = {
			
			picture = generic_army_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		TNG_alexander_von_falkenhausen = {
			
			picture = generic_army_asia_2
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}	
	}

	air_chief = {
		
		TNG_guillaume_soisson = {
			
			picture = generic_air_asia_3
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { air_air_combat_training_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		TNG_nicholas_diedrich = {
			
			picture = generic_air_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
	}

	navy_chief = {
		
		TNG_paul_medinger = {
			
			picture = generic_navy_asia_3
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		TNG_henri_ahnen = {
			
			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { navy_chief_maneuver_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	high_command = {

		TNG_francois_schammel = {
			ledger = army

			picture = generic_army_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		TNG_aloyse_glodt = {
			ledger = army

			picture = generic_army_asia_3
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		TNG_damien_roeser = {
			ledger = air

			picture = generic_air_asia_2
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { air_air_superiority_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		TNG_pierre_fischbach = {
			ledger = navy

			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = TNG
			}
			
			traits = { navy_fleet_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	theorist = {

		TNG_alex_federspiel = {
			ledger = army
					
			picture = generic_army_asia_1

			allowed = {
				original_tag = TNG
			}
			
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}

		TNG_nicolas_de_dixmude = {
			ledger = air
					
			picture = generic_air_asia_1
				
			allowed = {
				original_tag = TNG
			}
			
			research_bonus = {
				air_doctrine = 0.10
			}
			
			traits = { air_warfare_theorist }
		}

		TNG_florent_destriveaux = {
			ledger = navy
					
			picture = generic_navy_asia_1
				
			allowed = {
				original_tag = TNG
			}
			
			research_bonus = {
				naval_doctrine = 0.10
			}
			
			traits = { naval_theorist }
		}
	}

}