ideas = {
	political_advisor = {

		NSG_harold_ickes = {
			
			
			allowed = {
				original_tag = "NSG"
			}
			
			available = {
				is_subject = no
			}
			
			traits = { democratic_reformer }
	
			on_add = {
				#country_event = political.13
			}
	
			do_effect = {
				NOT = {
					has_government = democratic
				}
			}
			picture = generic_democratic_asia
			ai_will_do = {
				factor = 0
			}
		}
		
		NSG_charles_coughlin = {
			
			
			allowed = {
				original_tag = "NSG"
			}
			

			
			traits = { fascist_demagogue }
	
			on_add = {
				#country_event = political.7
			}
	
			do_effect = {
				NOT = {
					has_government = fascism
				}
			}
			picture = generic_fascist_asia
			ai_will_do = {
				factor = 0
			}
		}

		NSG_pierre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { silent_workhorse }
		}

		NSG_psre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { compassionate_gentleman }
		}

		NSG_pierre_krier = {

			picture = generic_political_advisor_asia_3
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { captain_of_industry }
		}
	}
	# MINSGARY
	army_chief = {
		
		NSG_emile_speller = {
			
			picture = generic_army_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		NSG_alexander_von_falkenhausen = {
			
			picture = generic_army_asia_2
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}	
	}

	air_chief = {
		
		NSG_guillaume_soisson = {
			
			picture = generic_air_asia_3
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { air_air_combat_training_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		NSG_nicholas_diedrich = {
			
			picture = generic_air_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
	}

	navy_chief = {
		
		NSG_paul_medinger = {
			
			picture = generic_navy_asia_3
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		NSG_henri_ahnen = {
			
			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { navy_chief_maneuver_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	high_command = {

		NSG_francois_schammel = {
			ledger = army

			picture = generic_army_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		NSG_aloyse_glodt = {
			ledger = army

			picture = generic_army_asia_3
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		NSG_damien_roeser = {
			ledger = air

			picture = generic_air_asia_2
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { air_air_superiority_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		NSG_pierre_fischbach = {
			ledger = navy

			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = NSG
			}
			
			traits = { navy_fleet_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	theorist = {

		NSG_alex_federspiel = {
			ledger = army
					
			picture = generic_army_asia_1

			allowed = {
				original_tag = NSG
			}
			
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}

		NSG_nicolas_de_dixmude = {
			ledger = air
					
			picture = generic_air_asia_1
				
			allowed = {
				original_tag = NSG
			}
			
			research_bonus = {
				air_doctrine = 0.10
			}
			
			traits = { air_warfare_theorist }
		}

		NSG_florent_destriveaux = {
			ledger = navy
					
			picture = generic_navy_asia_1
				
			allowed = {
				original_tag = NSG
			}
			
			research_bonus = {
				naval_doctrine = 0.10
			}
			
			traits = { naval_theorist }
		}
	}

}