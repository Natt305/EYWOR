ideas = {
	country = { 

		NTD_idea = {
			picture = generic_democratic_drift_bonus
			allowed = {
				always = no 
			}
			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.10
				industrial_capacity_factory = 0.10
			}
		}

		

		SINO_USA = {
			picture = generic_pp_unity_bonus
			allowed = {
				always = no 
			}
			removal_cost = -1
			
			modifier = {
				send_volunteer_size = 10
			}
		}

		STAG_idea = {
			picture = generic_usa_advisor
			allowed = {
				always = no 
			}
			removal_cost = -1
			
			modifier = {
				planning_speed = 0.1 
				max_planning_factor = 0.2
				training_time_factor = -0.2
				experience_gain_army = 0.02
				experience_gain_air = 0.02
				experience_gain_navy = 0.05
				amphibious_invasion = 0.15`
				invasion_preparation = -0.2
				naval_invasion_capacity = 15
				naval_invasion_penalty = -0.1
			}
		}

		White_group_idea = {
			picture = generic_jap_advisor
			allowed = {
				always = no 
			}
			removal_cost = -1
			
			modifier = {
				planning_speed = 0.1 
				max_planning_factor = 0.1
				experience_gain_army_factor = 0.1
				army_org_factor = 0.05
				army_core_attack_factor = 0.05
				army_core_defence_factor = 0.05
			}
		}

		MAAG_idea = {
			picture = CHI_MAAG
			allowed = {
				always = no 
			}
			removal_cost = -1
			
			modifier = {
				planning_speed = 0.2 
				max_planning_factor = 0.25
				training_time_factor = -0.2
				experience_gain_army = 0.05
				experience_gain_air = 0.05
				experience_gain_navy = 0.05
				amphibious_invasion = 0.15
				invasion_preparation = -0.2
				naval_invasion_capacity = 20
				naval_invasion_penalty = -0.2
				army_org_factor = 0.03
			}
		}


		


	}

}