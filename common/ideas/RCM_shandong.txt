ideas = {
	political_advisor = {
		
		SHD_pierre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { backroom_backstabber }
		}

		SHD_psre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { compassionate_gentleman }
		}

		SHD_pierre_krier = {

			picture = generic_political_advisor_asia_3
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { captain_of_industry }
		}
	}
	# MISHDARY
	army_chief = {
		
		SHD_emile_speller = {
			
			picture = generic_army_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		SHD_alexander_von_falkenhausen = {
			
			picture = generic_army_asia_2
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}	
	}

	air_chief = {
		
		SHD_guillaume_soisson = {
			
			picture = generic_air_asia_3
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { air_air_combat_training_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		SHD_nicholas_diedrich = {
			
			picture = generic_air_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
	}

	navy_chief = {
		
		SHD_paul_medinger = {
			
			picture = generic_navy_asia_3
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		SHD_henri_ahnen = {
			
			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { navy_chief_maneuver_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	high_command = {

		SHD_francois_schammel = {
			ledger = army

			picture = generic_army_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		SHD_aloyse_glodt = {
			ledger = army

			picture = generic_army_asia_3
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		SHD_damien_roeser = {
			ledger = air

			picture = generic_air_asia_2
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { air_air_superiority_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		SHD_pierre_fischbach = {
			ledger = navy

			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = SHD
			}
			
			traits = { navy_fleet_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	theorist = {

		SHD_alex_federspiel = {
			ledger = army
					
			picture = generic_army_asia_1

			allowed = {
				original_tag = SHD
			}
			
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}

		SHD_nicolas_de_dixmude = {
			ledger = air
					
			picture = generic_air_asia_1
				
			allowed = {
				original_tag = SHD
			}
			
			research_bonus = {
				air_doctrine = 0.10
			}
			
			traits = { air_warfare_theorist }
		}

		SHD_florent_destriveaux = {
			ledger = navy
					
			picture = generic_navy_asia_1
				
			allowed = {
				original_tag = SHD
			}
			
			research_bonus = {
				naval_doctrine = 0.10
			}
			
			traits = { naval_theorist }
		}
	}

}