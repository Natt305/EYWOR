PRC_request_SOV_aid_decision_cat = {
	PRC_request_SOV_aid_decision = {
		icon = infiltrate_state
		allowed = {
			is_literally_china = yes
		}
		target_trigger = {
			FROM = {
				tag = SOV
			}
		}
		visible = {
			has_completed_focus = PRC_request_supply_from_the_soviet_union_focus
		}
		available = {
			any_neighbor_country = {
				OR = {
					tag = FROM
					is_in_faction_with = FROM
					is_subject_of = FROM
				}
			}
			NOT = {
				has_war_with = FROM
			}
		}

		cost = 50
		days_remove = 120
		days_re_enable = 0
		#fire_only_once = yes

		modifier = {

		}

		remove_effect = {
			FROM = {
				country_event = RCM_comchina_zhang.1
			}
			custom_effect_tooltip = separate_line_tt
			effect_tooltip = {
				if = {
					limit = {
						FROM = {
							has_equipment = {
							    infantry_equipment > 1000
							}
						}
					}
					random_list = {
						30 = {
							country_event = RCM_comchina_zhang.2
						}
						30 = {
							country_event = RCM_comchina_zhang.3
						} 
						30 = {
							country_event = RCM_comchina_zhang.4
						} 
						10 = {
							country_event = RCM_comchina_zhang.5
						}
					}
				}
				else = {
					random_list = {
						30 = {
							country_event = RCM_comchina_zhang.3
						} 
						30 = {
							country_event = RCM_comchina_zhang.4
						} 
						10 = {
							country_event = RCM_comchina_zhang.5
						}
					}
				}
			}
		}
		complete_effect = {
			
		}
	}
}


#PRC decisions
parliament_gui_decision_category = {
	RCM_wang_ming_demand_meeting_mission = {
		icon = generic_nationalism
		fire_only_once = no
		days_mission_timeout = 140 # Stays for 5 days before being removed
		#days_re_enable = 25 # Will show up in the interface and can be selected again after 5 days
		#cost = 40

		is_good = no
		allowed = {
			tag = PRC
		}
		available = { #can do
			has_completed_focus = PRC_demand_another_politburo_meeting_focus
		}
		activation = {
			always = no #activate through previous decision
		}
		fixed_random_seed = no
		timeout_effect = { #run effect on selection
			country_event = RCM_comchina_decisions.7
			add_to_variable = {
				count_wang_ming = 1
			}
			effect_tooltip = {
				if = {
					limit = {
						check_variable = { count_wang_ming < 4 }
					}
					add_days_mission_timeout  = {
						mission = RCM_wang_ming_demand_meeting_mission
						days = -30
					}
				}
				set_temp_variable = { temp_member = 3 }
				set_temp_variable = { temp_seats = 0.035 tooltip = TEB_faction_support_tt }
										#percentage
				modify_parliament_seat = yes		
				add_stability = -0.035
			}
		}
		ai_will_do = {
			factor = 50
		}
	}
	RCM_minority_rule_mission = {
		icon = generic_nationalism
		fire_only_once = no
		days_mission_timeout = 140 # Stays for 5 days before being removed
		#days_re_enable = 25 # Will show up in the interface and can be selected again after 5 days
		#cost = 40

		is_good = no
		allowed = {
			tag = PRC
		}
		available = { #can do
			custom_trigger_tooltip = {
				tooltip = return_majority_tt
				NOT = {
					AND = {
						OR = {
							check_variable = { parliament_seat_array^0 < parliament_seat_array^1 }
							check_variable = { parliament_seat_array^0 < parliament_seat_array^2 }
							check_variable = { parliament_seat_array^0 < parliament_seat_array^3 }
						}
						has_country_leader = {
							name = "Zhang Guotao" ruling_only = yes
						}
					}


					AND = {
						OR = {
							check_variable = { parliament_seat_array^1 < parliament_seat_array^0 }
							check_variable = { parliament_seat_array^1 < parliament_seat_array^2 }
							check_variable = { parliament_seat_array^1 < parliament_seat_array^3 }
						}
						OR = {
							has_country_leader = {
								name = "Mao Zedong" ruling_only = yes
							}
							has_country_leader = {
								name = "Zhang Wentian" ruling_only = yes
							}
						}
					}


					AND = {
						OR = {
							check_variable = { parliament_seat_array^2 < parliament_seat_array^0 }
							check_variable = { parliament_seat_array^2 < parliament_seat_array^1 }
							check_variable = { parliament_seat_array^2 < parliament_seat_array^3 }
						}
						OR = {
							has_country_leader = {
								name = "Bo Gu" ruling_only = yes
							}
							has_country_leader = {
								name = "Wang Ming" ruling_only = yes
							}
						}
					}    

					AND = {
							OR = {
							check_variable = { parliament_seat_array^3 < parliament_seat_array^0 }
							check_variable = { parliament_seat_array^3 < parliament_seat_array^1 }
							check_variable = { parliament_seat_array^3 < parliament_seat_array^2 }
						}
						OR = {
							has_country_leader = {
								name = "Peng Shuzhi" ruling_only = yes
							}
							has_country_leader = {
								name = "Chen Duxiu" ruling_only = yes
							}
						}
					}
				}
			}
		}
		activation = {
			always = no #activate through previous decision
		}
		fixed_random_seed = no
		timeout_effect = { #run effect on selection
			country_event = RCM_comchina_decisions.2
			effect_tooltip = {		
				if = {
					limit = {
						has_stability < 0.15
					}
					if = {
						limit = {
							AND = {
								OR = {
									check_variable = { parliament_seat_array^0 < parliament_seat_array^1 }
									check_variable = { parliament_seat_array^0 < parliament_seat_array^2 }
									check_variable = { parliament_seat_array^0 < parliament_seat_array^3 }
								}
								has_country_leader = {
									name = "Zhang Guotao" ruling_only = yes
								}
							}
						}
						country_event = RCM_comchina_decisions.3
					}
					else_if = {
						limit = {
							AND = {
								OR = {
									check_variable = { parliament_seat_array^1 < parliament_seat_array^0 }
									check_variable = { parliament_seat_array^1 < parliament_seat_array^2 }
									check_variable = { parliament_seat_array^1 < parliament_seat_array^3 }
								}
								OR = {
									has_country_leader = {
										name = "Mao Zedong" ruling_only = yes
									}
									has_country_leader = {
										name = "Zhang Wentian" ruling_only = yes
									}
								}
							}
						}
						country_event = RCM_comchina_decisions.4
					}
					else_if = {
						limit = {
							AND = {
								OR = {
									check_variable = { parliament_seat_array^2 < parliament_seat_array^0 }
									check_variable = { parliament_seat_array^2 < parliament_seat_array^1 }
									check_variable = { parliament_seat_array^2 < parliament_seat_array^3 }
								}
								OR = {
									has_country_leader = {
										name = "Bo Gu" ruling_only = yes
									}
									has_country_leader = {
										name = "Wang Ming" ruling_only = yes
									}
								}
							}    
						}
						country_event = RCM_comchina_decisions.5
					}
					else_if = {
						limit = {
							AND = {
								OR = {
									check_variable = { parliament_seat_array^3 < parliament_seat_array^0 }
									check_variable = { parliament_seat_array^3 < parliament_seat_array^1 }
									check_variable = { parliament_seat_array^3 < parliament_seat_array^2 }
								}
								OR = {
									has_country_leader = {
										name = "Peng Shuzhi" ruling_only = yes
									}
									has_country_leader = {
										name = "Chen Duxiu" ruling_only = yes
									}
								}
							}
						}
						country_event = RCM_comchina_decisions.6
					}
				}		
				add_stability = -0.035
			}
		}
		ai_will_do = {
			factor = 50
		}
	}
	PRC_support_zhang_clique = {

		icon = eng_propaganda_campaigns
		#fire_only_once = yes
		
		available = {
			has_idea = PRC_zhang_guotao
		}
		#cost = 120
		custom_cost_trigger = {
			political_power_growth > 0.75
		}
		custom_cost_text = pp_per_day_neg_075
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = {
					AND = {
						OR = {
							check_variable = { parliament_seat_array^0 < parliament_seat_array^1 }
							check_variable = { parliament_seat_array^0 < parliament_seat_array^2 }
							check_variable = { parliament_seat_array^0 < parliament_seat_array^3 }
						}
						has_country_leader = {
							name = "Zhang Guotao" ruling_only = yes
						}
					}
				}
			}
		}
		days_remove = 270
		modifier = {
			political_power_gain = -0.75
		}
		visible = {
			tag = event_target:WTT_communist_china
		}
		remove_effect = {
			random_list = {
				15 = {
					set_temp_variable = { temp_member = 1 }
					set_temp_variable = { temp_seats = 0.03 tooltip = ZHANG_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				20 = {
					set_temp_variable = { temp_member = 1 }
					set_temp_variable = { temp_seats = 0.025 tooltip = ZHANG_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				15 = {
					set_temp_variable = { temp_member = 1 }
					set_temp_variable = { temp_seats = 0.02 tooltip = ZHANG_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
			}
		}
		complete_effect = {
			
		}
	}
	PRC_support_mao_clique = {

		icon = eng_propaganda_campaigns
		#fire_only_once = yes
		
		available = {
			has_idea_with_trait = maoists_trait
		}
		#cost = 120
		custom_cost_trigger = {
			political_power_growth > 0.75
		}
		custom_cost_text = pp_per_day_neg_075
		
		ai_will_do = {
			factor = 0 #no need
			#factor = 1
			#modifier = {
			#	factor = 0
			#	NOT = {
			#		AND = {
			#			OR = {
			#				check_variable = { parliament_seat_array^1 < parliament_seat_array^0 }
			#				check_variable = { parliament_seat_array^1 < parliament_seat_array^2 }
			#				check_variable = { parliament_seat_array^1 < parliament_seat_array^3 }
			#			}
			#			OR = {
			#				has_country_leader = {
			#					name = "Mao Zedong" ruling_only = yes
			#				}
			#				has_country_leader = {
			#					name = "Zhang Wentian" ruling_only = yes
			#				}
			#			}
			#		}
			#	}
			#}
		}
		days_remove = 270
		modifier = {
			political_power_gain = -0.75
		}
		visible = {
			tag = event_target:WTT_communist_china
		}
		remove_effect = {
			random_list = {
				15 = {
					set_temp_variable = { temp_member = 2 }
					set_temp_variable = { temp_seats = 0.03 tooltip = MAOIST_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				20 = {
					set_temp_variable = { temp_member = 2 }
					set_temp_variable = { temp_seats = 0.025 tooltip = MAOIST_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				15 = {
					set_temp_variable = { temp_member = 2 }
					set_temp_variable = { temp_seats = 0.02 tooltip = MAOIST_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
			}
		}
		complete_effect = {
			
		}
	}
	PRC_support_TEB_clique = {

		icon = eng_propaganda_campaigns
		#fire_only_once = yes
		
		available = {
			has_idea_with_trait = twenty_eight_bolshevik
		}
		#cost = 120
		custom_cost_trigger = {
			political_power_growth > 0.75
		}
		custom_cost_text = pp_per_day_neg_075
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = {
					AND = {
						OR = {
							check_variable = { parliament_seat_array^2 < parliament_seat_array^0 }
							check_variable = { parliament_seat_array^2 < parliament_seat_array^1 }
							check_variable = { parliament_seat_array^2 < parliament_seat_array^3 }
						}
						OR = {
							has_country_leader = {
								name = "Bo Gu" ruling_only = yes
							}
							has_country_leader = {
								name = "Wang Ming" ruling_only = yes
							}
						}
					}    
				}
			}
		}
		days_remove = 270
		modifier = {
			political_power_gain = -0.75
		}
		visible = {
			tag = event_target:WTT_communist_china
		}
		remove_effect = {
			random_list = {
				15 = {
					set_temp_variable = { temp_member = 3 }
					set_temp_variable = { temp_seats = 0.03 tooltip = TEB_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				20 = {
					set_temp_variable = { temp_member = 3 }
					set_temp_variable = { temp_seats = 0.025 tooltip = TEB_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				15 = {
					set_temp_variable = { temp_member = 3 }
					set_temp_variable = { temp_seats = 0.02 tooltip = TEB_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
			}
		}
		complete_effect = {
			
		}
	}
	PRC_support_TROT_clique = {

		icon = eng_propaganda_campaigns
		#fire_only_once = yes
		
		available = {
			OR = {
				has_idea_with_trait = devoted_trotskyist_RCM
				has_idea_with_trait = old_trotskyist_figurehead
				has_idea_with_trait = trotskyist_writer
			}
		}
		#cost = 120
		custom_cost_trigger = {
			political_power_growth > 0.75
		}
		custom_cost_text = pp_per_day_neg_075
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = {
					AND = {
						OR = {
							check_variable = { parliament_seat_array^3 < parliament_seat_array^0 }
							check_variable = { parliament_seat_array^3 < parliament_seat_array^1 }
							check_variable = { parliament_seat_array^3 < parliament_seat_array^2 }
						}
						OR = {
							has_country_leader = {
								name = "Peng Shuzhi" ruling_only = yes
							}
							has_country_leader = {
								name = "Chen Duxiu" ruling_only = yes
							}
						}
					}
				}
			}
		}
		days_remove = 270
		modifier = {
			political_power_gain = -0.75
		}
		visible = {
			tag = event_target:WTT_communist_china
		}
		remove_effect = {
			random_list = {
				15 = {
					set_temp_variable = { temp_member = 4 }
					set_temp_variable = { temp_seats = 0.03 tooltip = TROT_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				20 = {
					set_temp_variable = { temp_member = 4 }
					set_temp_variable = { temp_seats = 0.025 tooltip = TROT_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
				15 = {
					set_temp_variable = { temp_member = 4 }
					set_temp_variable = { temp_seats = 0.02 tooltip = TROT_faction_support_tt }
										#percentage
					modify_parliament_seat = yes
				}
			}
		}
		complete_effect = {
			
		}
	}

	#retification movement




	RCM_wang_shiwei_protest_mission = {
		icon = generic_political_rally
		fire_only_once = no
		days_mission_timeout = 180 # Stays for 5 days before being removed
		#days_re_enable = 25 # Will show up in the interface and can be selected again after 5 days
		#cost = 40

		is_good = no
		allowed = {
			tag = PRC
		}
		available = { #can do
			has_global_flag = wang_shiwei_is_put_on_trial
		}
		activation = {
			always = no #activate through previous decision
		}
		fixed_random_seed = no
		timeout_effect = { #run effect on selection
			country_event = RCM_comchina_mao.11
			add_to_variable = {
				count_wang_shiwei = 1
			}
			effect_tooltip = {
				if = {
					limit = {
						check_variable = { count_wang_shiwei < 8 }
					}
					add_days_mission_timeout  = {
						mission = RCM_wang_shiwei_protest_mission
						days = -15
					}
				}
			}
		}
		ai_will_do = {
			factor = 50
		}
	}
	RCM_core_member_protest_mission = {
		icon = generic_political_rally
		fire_only_once = no
		days_mission_timeout = 180 # Stays for 5 days before being removed
		#days_re_enable = 25 # Will show up in the interface and can be selected again after 5 days
		#cost = 40

		is_good = no
		allowed = {
			tag = PRC
		}
		available = { #can do
			has_global_flag = wang_ming_self_criticized
			has_global_flag = zhou_enlai_self_criticized
			has_global_flag = bo_gu_self_criticized
		}
		activation = {
			always = no #activate through previous decision
		}
		fixed_random_seed = no
		timeout_effect = { #run effect on selection
			country_event = RCM_comchina_mao.22
			add_to_variable = {
				count_wang_shiwei = 1
			}
			effect_tooltip = {
				if = {
					limit = {
						check_variable = { count_wang_shiwei < 8 }
					}
					add_days_mission_timeout  = {
						mission = RCM_core_member_protest_mission
						days = -15
					}
				}
			}
		}
		ai_will_do = {
			factor = 50
		}
	}
	RCM_PRC_set_up_general_study_committee = {
		icon = eng_trade_unions_support
		visible = {
			has_completed_focus = PRC_route_struggle_focus
		}
		available = {
			
		}
		fire_only_once = yes
		cost = 0
		days_remove = 30
		days_re_enable = 7
		fire_only_once = yes
		ai_will_do = {
			factor = 500
		}
		modifier = {
			
		}
	
		remove_effect = {
			country_event = RCM_comchina_mao.4			
			unlock_decision_tooltip = RCM_PRC_pushes_against_dogmatism
			unlock_decision_tooltip = RCM_PRC_push_against_empiricism
			unlock_decision_tooltip = RCM_PRC_push_against_sectarianism
			unlock_decision_tooltip = RCM_PRC_push_against_liberalism
			unlock_decision_tooltip = RCM_PRC_perform_speech_on_yanan_forum
			unlock_decision_tooltip = RCM_PRC_Conclude_Studies_in_each_departments
		}
		complete_effect = {
			custom_effect_tooltip = protests_will_arise_tt
			hidden_effect = {
				country_event = { id = RCM_comchina_mao.11 days = 15 random_days = 5 }
			}
		}
	}

	#[X] question our push against (subjectivism) Dogmatism (appear during decisions)
	RCM_PRC_pushes_against_dogmatism = {
		icon = generic_break_treaty
		visible = {
			has_global_flag = study_committee
		}
		available = {
			
		}
		fire_only_once = yes
		cost = 0
		days_remove = 40
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
		ai_will_do = {
			factor = 500
		}
		remove_effect = {
			set_global_flag = pushed_dogma_flag
			set_temp_variable = { temp_member = 3 }
			set_temp_variable = { temp_seats = -0.05 tooltip = TEB_faction_support_tt }
										#percentage
			modify_parliament_seat = yes
		}
		complete_effect = {
			custom_effect_tooltip = protests_will_arise_tt
			hidden_effect = {
				country_event = { id = RCM_comchina_mao.6 days = 20 random_days = 8 }
			}
		}
	}
	#[X] question our push against (subjectivism) empiricism (appear during decisions)
	RCM_PRC_push_against_empiricism = {
		icon = generic_break_treaty
		visible = {
			has_global_flag = study_committee
			
			
			
			
			
		}
		available = {
			has_global_flag = pushed_dogma_flag

		}
		fire_only_once = yes
		cost = 0
		days_remove = 40
		days_re_enable = 7
		fire_only_once = yes
		ai_will_do = {
			factor = 500
		}
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = pushed_empirical_flag
			add_political_power = 100
		}
		complete_effect = {
			custom_effect_tooltip = protests_will_arise_tt
			hidden_effect = {
				country_event = { id = RCM_comchina_mao.7 days = 20 random_days = 8 }
			}
		}
	}
	#Party members question our push against sectarianism (appear during decisions)
	RCM_PRC_push_against_sectarianism = {
		icon = generic_break_treaty
		visible = {
			has_global_flag = study_committee
		}
		available = {
			has_global_flag = pushed_empirical_flag

		}
		ai_will_do = {
			factor = 500
		}
		fire_only_once = yes
		cost = 0
		days_remove = 40
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = pushed_sect_flag
			add_stability = 0.05
		}
		complete_effect = {
			custom_effect_tooltip = protests_will_arise_tt
			hidden_effect = {
				country_event = { id = RCM_comchina_mao.8 days = 20 random_days = 8 }
			}
		}
	}
	#Party members question our push against liberalism (appear during decisions)
	RCM_PRC_push_against_liberalism = {
		icon = generic_break_treaty
		visible = {
			has_global_flag = study_committee
		}
		available = {
			has_global_flag = pushed_sect_flag

		}
		ai_will_do = {
			factor = 500
		}
		fire_only_once = yes
		cost = 0
		days_remove = 40
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = pushed_lib_flag
			set_temp_variable = { temp_member = 2 }
			set_temp_variable = { temp_seats = 0.05 tooltip = MAOIST_faction_support_tt }
										#percentage
			modify_parliament_seat = yes
		}
		complete_effect = {
			custom_effect_tooltip = protests_will_arise_tt
			hidden_effect = {
				country_event = { id = RCM_comchina_mao.9 days = 20 random_days = 8 }
			}
		}
	}
	#mao do the Talks on Yanan Forum (Wang shiwei be demonized)(after they criticized us)
	RCM_PRC_perform_speech_on_yanan_forum = {
		icon = hol_radio_oranje
		visible = {
			has_global_flag = study_committee
		}
		available = {
			has_global_flag = pushed_lib_flag

		}
		ai_will_do = {
			factor = 500
		}
		fire_only_once = yes
		cost = 0
		days_remove = 7
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = did_speech_flag
			country_event = RCM_comchina_mao.10
		}
		complete_effect = {
			
		}
	}
	#wang shiwei and other people question our party's oppression (pic needed)
	
	#"Conclude Studies in each departments"
	RCM_PRC_Conclude_Studies_in_each_departments = {
		icon = eng_trade_unions_demand
		visible = {
			has_global_flag = study_committee
		}
		available = {
			has_global_flag = did_speech_flag
		}
		ai_will_do = {
			factor = 500
		}
		fire_only_once = yes
		cost = 0
		days_remove = 20
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = first_phase_concluded
			unlock_decision_tooltip = RCM_PRC_mao_takeover_leadership
			unlock_decision_tooltip = RCM_PRC_initiate_the_rescue_campaign
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_mao_takeover_leadership = {
		icon = generic_military
		visible = {
			has_global_flag = first_phase_concluded
		}
		available = {
			
		}
	
		cost = 100
		#days_remove = 7
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			country_event = RCM_comchina_mao.12
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_initiate_the_rescue_campaign = {
		icon = generic_speech
		visible = {
			has_global_flag = first_phase_concluded
		}
		available = {
			
		}
	
		cost = 50
		days_remove = 20
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = rescue_campaign_initiated
			unlock_decision_tooltip = RCM_PRC_coersion_decsion
			unlock_decision_tooltip = RCM_PRC_purge_new_comers_decision
			unlock_decision_tooltip = RCM_PRC_trial_of_wang_shiwei
			unlock_decision_tooltip = RCM_PRC_force_wang_ming_admit_errors
			unlock_decision_tooltip = RCM_PRC_force_zhou_enlai_admit_errors
			unlock_decision_tooltip = RCM_PRC_force_bo_gu_admit_errors
		}
		complete_effect = {
			swap_ideas = {
				add_idea = yanan_rect_3
				remove_idea = yanan_rect_2
			}
			custom_effect_tooltip = protests_will_arise_tt
			hidden_effect = {
				country_event = { id = RCM_comchina_mao.22 days = 10 random_days = 3 }
			}
		}
	}
	RCM_PRC_coersion_decsion = {
		icon = generic_police_action
		visible = {
			has_global_flag = rescue_campaign_initiated
		}
		available = {
			
		}
	
		cost = 30
		days_remove = 50
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = confessions_coerced
			country_event = RCM_comchina_mao.14
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_purge_new_comers_decision = {
		icon = generic_assassination
		visible = {
			has_global_flag = rescue_campaign_initiated
		}
		available = {
			
		}
	
		cost = 20
		days_remove = 25
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = newcomers_purged
			country_event = RCM_comchina_mao.15
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_trial_of_wang_shiwei = {
		icon = generic_arrest
		visible = {
			has_global_flag = rescue_campaign_initiated
		}
		available = {
			
		}
	
		cost = 20
		days_remove = 30
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = wang_shiwei_is_put_on_trial
			country_event = RCM_comchina_mao.16
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_force_wang_ming_admit_errors = {
		icon = generic_political_discourse
		visible = {
			has_global_flag = rescue_campaign_initiated
		}
		available = {
			has_global_flag = newcomers_purged
			has_global_flag = confessions_coerced
			has_global_flag = wang_shiwei_is_put_on_trial
		}
	
		cost = 25
		days_remove = 25
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = wang_ming_self_criticized
			country_event = RCM_comchina_mao.18
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_force_zhou_enlai_admit_errors = {
		icon = generic_political_discourse
		visible = {
			has_global_flag = rescue_campaign_initiated
		}
		available = {
			has_global_flag = newcomers_purged
			has_global_flag = confessions_coerced
			has_global_flag = wang_shiwei_is_put_on_trial
		}
	
		cost = 25
		days_remove = 25
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
			
		}
	
		remove_effect = {
			set_global_flag = zhou_enlai_self_criticized
			country_event = RCM_comchina_mao.19
		}
		complete_effect = {
			
		}
	}
	RCM_PRC_force_bo_gu_admit_errors = {
		icon = generic_political_discourse
		visible = {
			has_global_flag = rescue_campaign_initiated
		}
		available = {
			has_global_flag = newcomers_purged
			has_global_flag = confessions_coerced
			has_global_flag = wang_shiwei_is_put_on_trial
		}
	
		cost = 25
		days_remove = 25
		days_re_enable = 7
		fire_only_once = yes
	
		modifier = {
		}
	
		remove_effect = {
			set_global_flag = bo_gu_self_criticized
			country_event = RCM_comchina_mao.21
		}
		complete_effect = {
			
		}
	}
}