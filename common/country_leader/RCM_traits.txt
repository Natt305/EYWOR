leader_traits = {
	#####MORDRED's Mongolia###########
	pillar_of_nation = {
		random = no
		fascism_drift = 0.1 #0.05
		neutrality_drift = 0.1 #0.05
	}
	intellectual = {
		random = no
		research_speed_factor = 0.02
	}
	cooperative_leader = {
		random = no
		political_power_cost = -0.1
		opinion_gain_monthly_same_ideology_factor = 1
	}
	staunch_traditionalist = {
		random = no
		drift_defence_factor = 0.5

		ai_will_do = {
			factor = 1
		}
	}
	spirit_of_genghis_improved = {
		random = no
		cavalry_attack_factor = 0.1
		cavalry_defence_factor = 0.1
		equipment_bonus = {
			nav_bomber_equipment = {
				instant = yes
				air_range = 0.1 naval_strike_attack = 0.2
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	###########MORDRED's Mongolia end############

	#COMPANY
	railway_conglomerate = {
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	transport_aircraft_manufacturer = {
		random = no
		equipment_bonus = {
			transport_plane_equipment = {
				air_range = 0.1
			}
			strat_bomber_equipment = {
				reliability = 0.2
			}
		}

		ai_will_do = {
			factor = 1
		}
	}
	RCM_construction_company = {
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	RCM_steel_works_2 = {
		industrial_capacity_dockyard = 0.05
		equipment_bonus = {
			infantry_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			armored_car_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			motorized_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			mechanized_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			amphibious_mechanized_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			artillery = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			anti_air_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			anti_tank_equipment = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
			armor = {
				reliability = 0.03
				build_cost_ic = -0.05
				instant = yes #steel isn't really the design
			}
		}
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	RCM_steel_works = {

		industrial_capacity_dockyard = 0.1
		equipment_bonus = {
			infantry_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			armored_car_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			motorized_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			mechanized_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			amphibious_mechanized_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			artillery = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			anti_air_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			anti_tank_equipment = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
			armor = {
				reliability = 0.05
				build_cost_ic = -0.1
				instant = yes #steel isn't really the design
			}
		}
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	RCM_machinery_manufacturer = {
		line_change_production_efficiency_factor = 0.15
		production_factory_efficiency_gain_factor = 0.05
		production_factory_start_efficiency_factor = 0.05
		production_factory_max_efficiency_factor = 0.05
		industrial_capacity_factory = 0.05
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	RCM_machinery_manufacturer_2 = {
		line_change_production_efficiency_factor = 0.3
		production_factory_efficiency_gain_factor = 0.05
		production_factory_start_efficiency_factor = 0.05
		production_factory_max_efficiency_factor = 0.05
		industrial_capacity_factory = 0.1
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	#########################
	xishan_doctrine = {
		random = no
		#terrain_penalty_reduction = 0.5
		stability_factor = 0.1
		army_org_factor = 0.1
		army_morale_factor = 0.15
		army_core_attack_factor = 0.10
		army_core_defence_factor = 0.15
		defensive_war_stability_factor = 0.4
		ai_will_do = {
			factor = 1
		}
	}
	#####################
	#JAP
	anti_axis_diplomat = {
		sprite = 13
		trade_opinion_factor = 0.075
		opinion_gain_monthly_factor = 0.075
		improve_relations_maintain_cost_factor = -0.25
		ai_will_do = {
			factor = 2
		}
	}
	#########################
	RCM_artillery_expert = { # 
		sprite = 8
		army_artillery_attack_factor = 0.15
		army_artillery_defence_factor = 0.1

		ai_will_do = {
			factor = 2
		}
	}
	RCM_morale_genius = {
		sprite = 5
		army_morale_factor = 0.12
		ai_will_do = {
			factor = 2
		}
	}
	RCM_infantry_expert = {
		sprite = 5
		army_infantry_attack_factor = 0.1
		army_infantry_defence_factor = 0.15
		ai_will_do = {
			factor = 3
		}
	}
	devil_of_showa = {
		random = no
		sprite = 10 #factory
		production_speed_buildings_factor = 0.1
		industrial_capacity_factory = 0.15
		stability_factor = 0.05
		war_support_factor = -0.2
		ai_will_do = {
			factor = 1
		}
	}
	imperial_tutor = {
		random = no
		sprite = 13
		research_speed_factor = 0.05
		#stability_factor = 0.05
		ai_will_do = {
			factor = 1
		}
	}
	trait_korean = {
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	trait_japanese = {
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	trait_manchu = {
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	trait_mongol = {
		random = no
		ai_will_do = {
			factor = 1
		}
	}
	royal_spymaster = {
		random = no
		sprite = 13
		decryption_factor = 0.15
		recon_factor = 0.1
		resistance_activity = -0.15
		resistance_growth = -0.15
		ai_will_do = {
			factor = 1
		}
	}
	SMR_general_director = {
		random = no
		sprite = 10 #factory
	 	mobilization_speed = 0.1
		land_reinforce_rate = 0.01
		production_speed_infrastructure_factor = 0.15
		ai_will_do = {
			factor = 1
		}
	}
	lawrence_of_manchuria = {
		random = no
		sprite = 7 #sword
		war_support_factor = 0.1
		ai_will_do = {
			factor = 1
		}
	}
	japan_manchuria_goodwill_ambassadress = {
		random = no
		sprite = 12 #star
		political_power_factor = 0.05
		stability_factor = 0.03
		war_support_factor = 0.03
		ai_will_do = {
			factor = 1
		}
	}
	elder_statesman = {
		random = no
		sprite = 13 #cabinet
		neutrality_drift = 0.05 #0.02
		research_speed_factor = 0.05
		stability_factor = 0.1
		ai_will_do = {
			factor = 1
		}
	}
	puyi_loyalist = {
		random = no
		sprite = 9 #sheild

		neutrality_drift = 0.05 #0.02
		drift_defence_factor = 0.15
		stability_factor = 0.1
		ai_will_do = {
			factor = 1
		}
	}
	bureaucratic_drug_lord = {
		random = no
		sprite = 10 #factory
		consumer_goods_factor = -0.15
		stability_factor = -0.05
		war_support_factor = -0.1
		ai_will_do = {
			factor = 1
		}
	}
	film_industry_tycoon = {
		random = no
		#sprite = 8 #tank
		sprite = 14 #cog
		drift_defence_factor = 0.2
		political_power_gain = 0.1
		war_support_factor = 0.05
		ai_will_do = {
			factor = 1
		}
	}
	white_bao_gong = {
		sprite = 13 #cabinet
		war_support_factor = 0.07
		stability_factor = 0.07
		ai_will_do = {
			factor = 1
		}
	}

	opposition_bureaucrat = {
		sprite = 13 #cabinet
		war_support_factor = -0.1
		stability_factor = -0.05
		ai_will_do = {
			factor = 1
		}
	}
	naval_expansionist = {
		sprite = 10 #factory
		industrial_capacity_factory = -0.05
		industrial_capacity_dockyard = 0.05
		ai_will_do = {
			factor = 1
		}
	}
	
	################
	empire_restored = {
		random = no
		political_power_gain = 0.05
		war_support_factor = 0.15
		ai_will_do = {
			factor = 1
		}
	}

	com_sympathizer = {
		random = no
		communism_acceptance = 85
		ai_will_do = {
			factor = 1
		}
	}

	socialist_idealist = {
		random = no
		# Boosts Democratic socialism
		sprite = 13
		n_socialism_drift = 0.1 #0.05
		ai_will_do = {
			factor = 0
		}
	}

	fascist_demagogue = {
		random = no
		# Boosts Fascism
		sprite = 13
		fascism_drift = 0.1 #0.05

		ai_will_do = {
			factor = 0
		}
	}

	pan_asianist = {
		sprite = 13
		ai_will_do = {
			factor = 1
		}
	}

	genro = {
		sprite = 13
		stability_factor = 0.08
		political_power_factor = 0.12
		ai_will_do = {
			factor = 1
		}
	}


	strategic_mastermind = {
		sprite = 13
		experience_gain_army = 0.05
		ai_will_do = {
			factor = 1
		}
	}


	wartime_propagantist = {
		sprite = 13
		random = no
		war_support_factor = 0.1
		#defensive_war_stability_factor = 0.15
		#offensive_war_stability_factor = 0.15
		justify_war_goal_time = -0.25
		generate_wargoal_tension = -0.25
		enemy_justify_war_goal_time = -0.75
		ai_will_do = {
			factor = 1
		}
	}
	staunch_patriot = {
		sprite = 13
		random = no
		fascism_drift = 0.01 #0.01
		neutrality_drift = 0.02 #0.02
		war_support_factor = 0.1
		min_export = -0.1
		join_faction_tension = 0.25
		opinion_gain_monthly_factor = -0.75
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					is_subject = yes
					is_in_faction_with = SOV
					is_in_faction_with = ENG
					is_in_faction_with = USA
				}
			}
		}
	}
	progressive_activist = {
		sprite = 13
		random = no
		n_socialism_drift = 0.01 #0.01
		democratic_drift = 0.02 #0.02
		guarantee_tension = -0.20
		research_speed_factor = 0.25
		war_support_weekly = -0.002
		justify_war_goal_time = 0.35
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					is_subject = yes
					is_in_faction_with = SOV
					is_in_faction_with = ITA
					is_in_faction_with = GER
				}
			}
		}
	}
	revolutionary_syndicalist = {
		sprite = 13
		random = no
		n_socialism_drift = 0.01 #0.01
		communism_drift = 0.02 #0.02
		war_support_factor = -0.05
		industrial_capacity_factory = 0.15
		consumer_goods_factor = 0.1
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					is_subject = yes
					is_in_faction_with = ENG
					is_in_faction_with = ITA
					is_in_faction_with = GER
					is_in_faction_with = USA
				}
			}
		}
	}
	robber_baron = {
		sprite = 10
		random = no
		#n_socialism_drift = -0.02
		#communism_drift = 0.01 #0.01
		industrial_capacity_factory = 0.10
		stability_factor = -0.05
		war_support_factor = -0.05
		ai_will_do = {
			factor = 1
		}
	}


	mass_infantry_equipment_manufacturer = {
		random = no
		equipment_bonus = {
			infantry_equipment = {
				reliability = -0.05
				build_cost_ic = -0.1
				#instant = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	bad_infantry_equipment_manufacturer = {
		random = no
		equipment_bonus = {
			infantry_equipment = {
				soft_attack = -0.05
				reliability = -0.05
				build_cost_ic = -0.15
				#instant = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	slow_infantry_equipment_manufacturer = {
		random = no
		equipment_bonus = {
			infantry_equipment = {
				soft_attack = 0.1
				reliability = 0.1
				build_cost_ic = 0.1
				#instant = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	passive_figurehead = {
		ai_will_do = {
			factor = 1
		}
		stability_factor = 0.10
		political_power_factor = -0.10
		
	}
	
	son_of_heaven = {
		random = no
		ai_will_do = {
			factor = 1
		}
		stability_factor = 0.15
		war_support_factor = 0.15
		political_power_factor = 0.1
		political_advisor_cost_factor = -0.15
		trade_laws_cost_factor = -0.15
		mobilization_laws_cost_factor = -0.15
		economy_cost_factor = -0.15
		high_command_cost_factor = -0.15
		air_chief_cost_factor = -0.15
		army_chief_cost_factor = -0.15
		navy_chief_cost_factor = -0.15
	}
	
	imperial_rubber_stamp = {
		random = no
		ai_will_do = {
			factor = 1
		}
		stability_factor = 0.2
	}


	naval_advisory_team = {
		experience_gain_navy = 0.05

		ai_will_do = {
			factor = 3
		}
	}
	battle_plan_mission = {
		experience_gain_navy = 0.05
		ai_will_do = {
			factor = 3
		}
	}
	military_advisory_team = {
		experience_gain_army = 0.05

		ai_will_do = {
			factor = 3
		}
	}
	mobile_warfare_mission = {
		experience_gain_army = 0.05
		ai_will_do = {
			factor = 3
		}
	}
	field_artillery_specialists = {
		experience_gain_army = 0.05

		ai_will_do = {
			factor = 3
		}
	}
	mass_assault_mission = {
		experience_gain_army = 0.05

		ai_will_do = {
			factor = 3
		}
	}
	firepower_advisory_team = {
		experience_gain_army = 0.05

		ai_will_do = {
			factor = 3
		}
	}
	firepower_advisory_team = {
		experience_gain_army = 0.05

		ai_will_do = {
			factor = 3
		}
	}
	manufacturers_agents = {
		license_purchase_cost = -0.25
		license_tech_difference_speed = 0.1
		ai_will_do = {
			factor = 9
		}
	}
	no_inflation_risk = {
		ai_will_do = {
			factor = 1
		}
	}
	low_inflation_risk = {
		ai_will_do = {
			factor = 1
		}
	}
	moderate_inflation_risk = {
		ai_will_do = {
			factor = 1
		}
	}
	considerable_inflation_risk = {
		ai_will_do = {
			factor = 1
		}
	}
	high_inflation_risk = {
		ai_will_do = {
			factor = 1
		}
	}
	inflation_on_crises = {
		ai_will_do = {
			factor = 1
		}
	}
	
	plus_point_2_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	plus_point_1_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	minus_point_1_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	minus_point_2_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	minus_point_3_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	minus_point_4_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	minus_point_5_per_week = {
		ai_will_do = {
			factor = 1
		}
	}
	SC_cap_90_percent = {
		ai_will_do = {
			factor = 1
		}
	}
	SC_cap_80_percent = {
		ai_will_do = {
			factor = 1
		}
	}
	SC_cap_70_percent = {
		ai_will_do = {
			factor = 1
		}
	}
	SC_cap_60_percent = {
		ai_will_do = {
			factor = 1
		}
	}
	neg_40_percent_modifier = {
		ai_will_do = {
			factor = 1
		}
	}
	neg_30_percent_modifier = {
		ai_will_do = {
			factor = 1
		}
	}
	neg_20_percent_modifier = {
		ai_will_do = {
			factor = 1
		}
	}
	neg_10_percent_modifier = {
		ai_will_do = {
			factor = 1
		}
	}
	send_guns_by_a_massive_amount = {
		ai_will_do = {
			factor = 1
		}
	}
	#MAN
	goes_away_below_3_percent = {
		ai_will_do = {
			factor = 1
		}
	}
	stikes_will_be_frequent_when_above_40_percent = {
		ai_will_do = {
			factor = 1
		}
	}

	#seven gentle men
	prominent_lawyer = {
		ai_will_do = {
			factor = 1
		}
		sprite = 13
		stability_weekly = 0.001
		political_power_factor = -0.05		
	}
	socialist_novelist_RCM = {
		random = no
		sprite = 12

		war_support_factor = 0.1
		n_socialism_drift = 0.1 #0.05

		ai_will_do = {
			factor = 1
		}
	}

	#MACARTHUR
	refounder_of_japan = {
		random = no
	    stability_factor = 0.05
		trade_opinion_factor = 0.2
		ai_will_do = {
			factor = 1
		}
	}

	#sol jap
	christian_socialist = {
		#sprite = 12
		random = no
		custom_modifier_tooltip = christian_socialist_mod_tt
		ai_will_do = {
			factor = 1
		}
	}
	dockyard_union_boss = {
		sprite = 10
		random = no
		#n_socialism_drift = -0.02
		#communism_drift = 0.01 #0.01
		
		production_speed_dockyard_factor = -0.05
		stability_factor = 0.05
		political_power_gain = 0.1
		ai_will_do = {
			factor = 1
		}
	}
	father_of_japanese_baseball = {
		random = no
		#n_socialism_drift = -0.02
		#communism_drift = 0.01 #0.01
		
		#production_speed_dockyard_factor = -0.05
		stability_factor = 0.05
		#political_power_gain = 0.1
		ai_will_do = {
			factor = 1
		}
	}
	
	devoted_trotskyist_RCM = { #Hernan Laborde Devoted Trotkyist
		random = no
		sprite = 13
		war_support_factor = 0.05
		subversive_activites_upkeep = -0.25

		ai_will_do = {
			factor = 1
		}
	}
	trotskyist_philosopher = {
		random = no
		sprite = 13
		#communism_drift = 0.05
		political_power_factor = 0.1
		research_speed_factor = 0.05
	}
	big_brother = {
		random = no
		#n_socialism_drift = -0.02
		#communism_drift = 0.01 #0.01
		drift_defence_factor = 0.35
		political_power_factor = 0.2
		resistance_growth = -0.35
		resistance_target = -0.35
		resistance_damage_to_garrison = -0.35 
		foreign_subversive_activites = -0.35
		non_core_manpower = 0.25
		operation_infiltrate_outcome = 0.35
		boost_resistance_factor = 0.35
		propaganda_mission_factor = 0.35
		boost_ideology_mission_factor = 0.35
		operative_slot = 2
		agency_upgrade_time = -0.35
		#production_speed_dockyard_factor = -0.05
		#stability_factor = 0.05
		#political_power_gain = 0.1
		ai_will_do = {
			factor = 1
		}
	}
	old_gaurd_rcm_trait = {
		random = no
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	appeal_trait = {
		random = no
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	militant_rcm_trait = {
		random = no
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	comintern_celebrity = {
		random = no
		sprite = 13
		opinion_gain_monthly_same_ideology_factor = 0.5
		stability_factor = 0.05
		war_support_factor = 0.05
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	agrarian_reformer = {
		random = no
		sprite = 10
		consumer_goods_factor = 0.02
		global_building_slots_factor = 0.15
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	trotskyist_writer = {
		random = no
		sprite = 13
		#communism_drift = 0.05
		stability_factor = 0.05


		#political_power_factor = 0.1	
	}
	twenty_eight_bolshevik = {
		random = no
		custom_modifier_tooltip = TEB_faction_monthly_tt
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	maoists_trait = {
		random = no
		custom_modifier_tooltip = MAO_faction_monthly_tt
	}
	former_twenty_eight_bolshevik = {
		random = no
		#sprite = 13
		#communism_drift = 0.05
		#political_power_factor = 0.1
		#research_speed_factor = 0.05
	}
	RCM_propaganda_expert = {
		sprite = 13
		war_support_factor = 0.10

		ai_will_do = {
			factor = 1
		}
	}
	social_philosopher = {
		sprite = 12
		n_socialism_drift = 0.05
		political_power_factor = 0.1
		research_speed_factor = 0.05

		ai_will_do = {
			factor = 1
		}
	}
	party_organizer = {
		sprite = 10
		random = yes
		political_power_factor = 0.05
		party_popularity_stability_factor = 0.1 #MODDED
		#production_factory_max_efficiency_factor = 0.02
		#consumer_goods_factor = -0.02
	}
	old_trotskyist_figurehead = {
		sprite = 12
		random = yes
		political_power_factor = 0.05
		subversive_activites_upkeep = -0.25 #MODDED
		#production_factory_max_efficiency_factor = 0.02
		#consumer_goods_factor = -0.02
	}

	blue_shirts_ideologue = {
		random = no
		# Boosts Fascism
		sprite = 13
		fascism_drift = 0.1

		ai_will_do = {
			factor = 1
		}
	}
	japanese_shina_tsu = { #Shina-tsū
		random = no
		# Boosts Fascism
		sprite = 13
		fascism_drift = 0.1

		ai_will_do = {
			factor = 1
		}
	}
	opposition_revolutionary = {
		random = no
		# Boosts Communism
		sprite = 13
		communism_drift = 0.1

		ai_will_do = {
			factor = 1
		}
	}
	guerilla_warfare_strategist = {
		random = no
		# Boosts Communism
		sprite = 13
		command_power_gain_mult = 0.1
		max_command_power = 20
		ai_will_do = {
			factor = 1
		}
	}

	#PRC
	red_sun_of_the_people = {
		random = no
	    stability_factor = 0.05
		communism_drift = 0.15
		war_support_factor = 0.15
		political_advisor_cost_factor = -0.07 #MODDED
		army_chief_cost_factor = -0.07 #MODDED
		navy_chief_cost_factor = -0.07 #MODDED
		air_chief_cost_factor = -0.07 #MODDED
		theorist_cost_factor = -0.07 #MODDED
		high_command_cost_factor = -0.07 #MODDED
		ai_will_do = {
			factor = 1
		}
	}

	TEB_faction_monthly_tt_trait = {
		random = no
	}
	TEB_faction_1_monthly_tt_trait = {
		random = no
	}
	can_justify_war_on_non_threat_country_TT = {
		random = no
	}
}
